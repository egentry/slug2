<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    <title>Introduction to SLUG &mdash; slug 2.0 documentation</title>
    
    <link rel="stylesheet" href="_static/alabaster.css" type="text/css" />
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
    
    <script type="text/javascript">
      var DOCUMENTATION_OPTIONS = {
        URL_ROOT:    './',
        VERSION:     '2.0',
        COLLAPSE_INDEX: false,
        FILE_SUFFIX: '.html',
        HAS_SOURCE:  true
      };
    </script>
    <script type="text/javascript" src="_static/jquery.js"></script>
    <script type="text/javascript" src="_static/underscore.js"></script>
    <script type="text/javascript" src="_static/doctools.js"></script>
    <script type="text/javascript" src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
    <link rel="top" title="slug 2.0 documentation" href="index.html" />
    <link rel="next" title="Compiling and Installing SLUG" href="compiling.html" />
    <link rel="prev" title="Getting SLUG" href="getting.html" />
   
  
  <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9">

  </head>
  <body role="document">
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="py-modindex.html" title="Python Module Index"
             >modules</a> |</li>
        <li class="right" >
          <a href="compiling.html" title="Compiling and Installing SLUG"
             accesskey="N">next</a> |</li>
        <li class="right" >
          <a href="getting.html" title="Getting SLUG"
             accesskey="P">previous</a> |</li>
        <li class="nav-item nav-item-0"><a href="index.html">slug 2.0 documentation</a> &raquo;</li> 
      </ul>
    </div>  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <div class="section" id="introduction-to-slug">
<h1>Introduction to SLUG<a class="headerlink" href="#introduction-to-slug" title="Permalink to this headline">¶</a></h1>
<p>This is a guide for users of the SLUG software package. SLUG is distributed under the terms of the <a class="reference external" href="http://www.gnu.org/licenses/gpl.html">GNU General Public License v. 3.0</a>. A copy of the license notification is included in the main SLUG directory. If you use SLUG in any published work, please cite the SLUG method papers, <a class="reference external" href="http://adsabs.harvard.edu/abs/2012ApJ...745..145D">da Silva, R. L., Fumagalli, M., &amp; Krumholz, M. R., 2012, The Astrophysical Journal, 745, 145</a> and <a class="reference external" href="http://adsabs.harvard.edu/abs/2015MNRAS.452.1447K">Krumholz, M. R., Fumagalli, M., da Silva, R. L., Rendahl, T., &amp; Parra, J. 2015, Monthly Notices of the Royal Astronomical Society, 452, 1447</a>.</p>
<div class="section" id="what-does-slug-do">
<h2>What Does SLUG Do?<a class="headerlink" href="#what-does-slug-do" title="Permalink to this headline">¶</a></h2>
<p>SLUG is a stellar population synthesis (SPS) code, meaning that, for a specified stellar initial mass function (IMF), star formation history (SFH), cluster mass function (CMF), cluster lifetime function (CLF), and (optionally) distribution of extinctions (A_V), it predicts the spectra and photometry of both individual star clusters and the galaxies (or sub-regions of galaxies) that contain them. In this regard, SLUG operates much like any other SPS code. The main difference is that SLUG regards the functions describing the stellar population as probability distributions, and the resulting stellar population as being the result of a draw from them. SLUG performs a Monte Carlo simulation to determine the PDF of the light produced by the stellar populations that are drawn from these distributions. The remainder of this section briefly describes the major conceptual pieces of a SLUG simulation. For a more detailed description, readers are referred to <a class="reference external" href="http://adsabs.harvard.edu/abs/2012ApJ...745..145D">da Silva, Fumagalli, &amp; Krumholz (2012)</a>.</p>
</div>
<div class="section" id="cluster-simulations-and-galaxy-simulations">
<h2>Cluster Simulations and Galaxy Simulations<a class="headerlink" href="#cluster-simulations-and-galaxy-simulations" title="Permalink to this headline">¶</a></h2>
<p>SLUG can simulate either a simple stellar population (i.e., a group of stars all born at one time) or a composite stellar population, consisting of stars born at a distribution of times. We refer to the former case as a &#8220;cluster&#8221; simulation, and the latter as a &#8220;galaxy&#8221; simulation, since one can be thought of as approximating the behavior of a single star cluster, and the other as approximating a whole galaxy.</p>
</div>
<div class="section" id="probability-distribution-functions-the-imf-sfh-cmf-clf-a-v-distribution">
<span id="ssec-slugpdfs"></span><h2>Probability Distribution Functions: the IMF, SFH, CMF, CLF, A_V distribution<a class="headerlink" href="#probability-distribution-functions-the-imf-sfh-cmf-clf-a-v-distribution" title="Permalink to this headline">¶</a></h2>
<p>As mentioned above, SLUG regards the IMF, SFH, CMF, CLF, and extinction A_V as probability distribution functions. These PDFs can be described by a very wide range of possible functional forms; see <a class="reference internal" href="pdfs.html#sec-pdfs"><span>Probability Distribution Functions</span></a> for details on the exact functional forms allowed, and on how they can be specified in the code. When SLUG runs a cluster simulation, it draws stars from the specified IMF in an attempt to produce a cluster of a user-specified total mass. There are a number of possible methods for performing such mass-limited sampling, and SLUG gives the user a wide menu of options; see <a class="reference internal" href="pdfs.html#sec-pdfs"><span>Probability Distribution Functions</span></a>. SLUG will also, upon user request, randomly draw a visual extinction A_V to be applied to the light.</p>
<p>For a galaxy simulation, the procedure involves one extra step. In this case, SLUG assumes that some fraction <span class="math">\(f_c\)</span> of the stars in the galaxy are born in star clusters, which, for the purposes of SLUG, means that they all share the same birth time. The remaining fraction <span class="math">\(1-f_c\)</span> of stars are field stars. When a galaxy simulation is run, SLUG determines the total mass of stars <span class="math">\(M_*\)</span> that should have formed since the start of the simulation (or since the last output, if more than one output is requested) from the star formation history, and then draws field stars and star clusters in an attempt to produce masses <span class="math">\((1-f_c)M_*\)</span> and <span class="math">\(f_c M_*\)</span>. For the field stars, the stellar masses are drawn from the IMF, in a process completely analogous to the cluster case, and each star is given its own randomly-generated extinction. For star clusters, the masses of the clusters are drawn from the CMF, and each cluster is then populated from the IMF as in the cluster case. Again, each cluster gets its own extinction. For both the field stars and the star clusters, the time of their birth is drawn from the PDF describing the SFH.</p>
<p>Finally, star clusters can be disrupted independent of the fate of their parent stars. When each cluster is formed, it is assigned a lifetime drawn from the CLF. Once that time has passed, the cluster ceases to be entered in the lists of individual cluster spectra and photometry (see next section), although the individual stars continue to contribute to the integrated light of the galaxy.</p>
</div>
<div class="section" id="spectra-and-photometry">
<span id="ssec-spec-phot"></span><h2>Spectra and Photometry<a class="headerlink" href="#spectra-and-photometry" title="Permalink to this headline">¶</a></h2>
<p>Once SLUG has drawn a population of stars, its final step is to compute the light they produce. SLUG does this in several steps. First, it computes the physical properties of all the stars present user-specified times using a set of stellar evolutionary tracks. Second, it uses these physical properties to compute the composite spectra produced by the stars, using a user-specified set of stellar atmosphere models. Formally, the quantity computed is the specific luminosity per unit wavelength <span class="math">\(L_\lambda\)</span>. Third, if nebular emission is enabled, the code calculates the spectrum <span class="math">\(L_{\lambda,\mathrm{neb}}\)</span> that emerges after the starlight passes through the HII region aruond the star &#8211; see <a class="reference internal" href="#ssec-nebula"><span>Nebular Processing</span></a>. Fourth, if extinction is enabled, SLUG computes the extincted stellar and nebula-processed spectra <span class="math">\(L_{\lambda,\mathrm{ex}}\)</span> and <span class="math">\(L_{\lambda,\mathrm{neb,ex}}\)</span> &#8211; see <a class="reference internal" href="#ssec-extinction"><span>Extinction</span></a>. Fifth and finally, SLUG computes photometry for the stellar population by integrating all computed spectra over a set of specified photometric filters. Depending on the options specified by the user and the filter under consideration, the photometric value output will be one of the following:</p>
<ul class="simple">
<li>The frequency-averaged luminosity across the filter, defined as</li>
</ul>
<div class="math">
\[\langle L_\nu\rangle_R = \frac{\int L_\nu \, d\ln\nu}{\int R_\nu (\nu/\nu_c)^\beta \, d\ln\nu},\]</div>
<p>where <span class="math">\(L_\nu\)</span> is the specific luminosity per unit frequency, <span class="math">\(R_\nu\)</span> is the filter response function per photon at frequency <span class="math">\(\nu\)</span>, <span class="math">\(\nu_c\)</span> is the central wavelength of the filter, and <span class="math">\(\beta\)</span> is a constant that is defined by convention for each filter, and is either 0, 1, or 2; usually it is 0 for optical and UV filters.</p>
<ul class="simple">
<li>The wavelength-averaged luminosity across the filter, defined as</li>
</ul>
<div class="math">
\[\langle L_\lambda\rangle_R = \frac{\int L_\lambda \, d\ln\lambda}{\int R_\lambda (\lambda/\lambda_c)^{-\beta} \, d\ln\lambda},\]</div>
<p>where <span class="math">\(L_\lambda\)</span> is the specific luminosity per unit wavelength, <span class="math">\(R_\lambda\)</span> is the filter response function per photon at wavelength <span class="math">\(\lambda\)</span>, and <span class="math">\(\lambda_c\)</span> is the central wavelength of the filter.</p>
<ul class="simple">
<li>The AB magnitude, defined by</li>
</ul>
<div class="math">
\[M_{\rm AB} = -2.5 \log_{10} \left[\frac{\langle L_\nu\rangle_R}{4\pi\left(10\,\mathrm{pc}\right)^2}\right] - 48.6,\]</div>
<p>where <span class="math">\(\langle L_\nu\rangle_R\)</span> is in units of <span class="math">\(\mathrm{erg\,s}^{-1}\,\mathrm{Hz}^{-1}\)</span>.</p>
<ul class="simple">
<li>The ST magnitude, defined by</li>
</ul>
<div class="math">
\[M_{\rm ST} = -2.5 \log_{10} \left[\frac{\langle L_\lambda\rangle_R}{4\pi\left(10\,\mathrm{pc}\right)^2}\right] - 21.1,\]</div>
<p>where <span class="math">\(\langle L_\lambda\rangle_R\)</span> is in units of <span class="math">\(\mathrm{erg\, s}^{-1}\,\mathrm{Angstrom}^{-1}\)</span>.</p>
<ul class="simple">
<li>The Vega magnitude, defined by</li>
</ul>
<div class="math">
\[M_{\rm Vega} = M_{\rm AB} - M_{\rm AB}(\mbox{Vega}),\]</div>
<p>where <span class="math">\(M_{\rm AB}(\mbox{Vega})\)</span> is the AB magnitude of Vega. The latter quantity is computed on the fly, using a stored Kurucz model spectrum for Vega.</p>
<ul class="simple">
<li>The photon flux above some threshold <span class="math">\(\nu_0\)</span>, defined as</li>
</ul>
<div class="math">
\[Q(\nu_0) = \int_{\nu_0}^\infty \frac{L_\nu}{h\nu} \, d\nu.\]</div>
<ul class="simple">
<li>The bolometric luminosity,</li>
</ul>
<div class="math">
\[L_{\rm bol} = \int_0^\infty L_\nu \, d\nu.\]</div>
<p>If nebular processing and/or extinction are enabled, photometric quantities are computed separately for each available version of the spectrum, <span class="math">\(L_\lambda\)</span>, <span class="math">\(L_{\lambda,\mathrm{neb}}\)</span>, <span class="math">\(L_{\lambda,\mathrm{ex}}\)</span>, and <span class="math">\(L_{\lambda,\mathrm{neb,ex}}\)</span>.</p>
<p>For a cluster simulation, this procedure is applied to the star cluster being simulated at a user-specified set of output times. For a galaxy simulation, the procedure is much the same, but it can be done both for all the stars in the galaxy taken as a whole, and individually for each star cluster that is still present (i.e., that has not been disrupted).</p>
</div>
<div class="section" id="monte-carlo-simulation">
<h2>Monte Carlo Simulation<a class="headerlink" href="#monte-carlo-simulation" title="Permalink to this headline">¶</a></h2>
<p>The steps described in the previous two section are those required for a single realization of the stellar population. However, the entire point of SLUG is to repeat this procedure many times in order to build up the statistics of the population light output. Thus the entire procedure can be repeated as many times as the user desires.</p>
</div>
<div class="section" id="nebular-processing">
<span id="ssec-nebula"></span><h2>Nebular Processing<a class="headerlink" href="#nebular-processing" title="Permalink to this headline">¶</a></h2>
<p>SLUG includes methods for post-processing the output starlight to compute the light that will emerge from the HII region around star clusters, and to further apply extinction to that light.</p>
<p>Nebular emission is computed by assuming that, for stars / star clusters younger than 10 Myr, all the ionizing photons are absorbed in a uniform-density, uniform-temperature HII region around each star cluster / star, and then computing the resulting emission at non-ionizing energies. The calculation assumes that the HII region is in photoionization equilibrium, and consists of hydrogen that is fully ionized and helium that is singly ionized. Under these assumptions the volume <span class="math">\(V\)</span>, electron density <span class="math">\(n_e\)</span>, and hydrogen density <span class="math">\(n_{\mathrm{H}}\)</span> are related to the hydrogen ionizing luminosity <span class="math">\(Q(\mathrm{H}^0)\)</span> via</p>
<div class="math">
\[\phi Q(\mathrm{H}^0) = \alpha_{\mathrm{B}}(T) n_e n_{\mathrm{H}} V\]</div>
<p>Here <span class="math">\(\phi\)</span> is the fraction of ionizing photons that are absorbed by hydrogen rather than dust grains, and <span class="math">\(\alpha_{\mathrm{B}}(T)\)</span> is the temperature-dependent case B recombination rate coefficient. SLUG approximates <span class="math">\(\alpha_{\mathrm{B}}(T)\)</span> using the analytic approximation given by equation 14.6 of <a class="reference external" href="http://adsabs.harvard.edu/abs/2011piim.book.....D">Draine (2011, Physics of the Interstellar and Intergalactic Medium, Princeton University Press)</a>, while <span class="math">\(\phi\)</span> is a user-chosen parameter. The temperature can either be set by the user directly, or can be looked up automatically based on the age of the stellar population.</p>
<p>The relation above determines <span class="math">\(n_e n_{\mathrm{H}} V\)</span>, and from this SLUG computes the nebular emission including the following processes:</p>
<ul class="simple">
<li><span class="math">\(\mathrm{H}^+\)</span> and <span class="math">\(\mathrm{He}^+\)</span> free-free emission</li>
<li><span class="math">\(\mathrm{H}\)</span> and <span class="math">\(\mathrm{He}\)</span> bound-free emission</li>
<li>Hydrogen 2-photon emission</li>
<li>Hydrogen recombination lines from all lines with upper levels <span class="math">\(n_u \leq 25\)</span></li>
<li>Non-hydrogen line emission based on a tabulation (see below)</li>
</ul>
<p>Formally, the luminosity per unit wavelength is computed as</p>
<div class="math">
\[\begin{split}L_{\lambda,\mathrm{neb}} = \left[\gamma_{\mathrm{ff}}^{(\mathrm{H})} + \gamma_{\mathrm{bf}}^{(\mathrm{H})} + \gamma_{\mathrm{2p}}^{(\mathrm{H})} + \sum_{n,n' \leq 25, n&lt;n'} \alpha_{nn'}^{\mathrm{eff,B,(H)}} E_{nn'}^{(\mathrm{H})} +  x_{\mathrm{He}} \gamma_{\mathrm{ff}}^{(\mathrm{He})} +  x_{\mathrm{He}} \gamma_{\mathrm{bf}}^{(\mathrm{He})} + \sum_i \gamma_{i,\mathrm{line}}^{(\mathrm{M})}\right] n_e n_{\mathrm{H}}{V}\end{split}\]</div>
<p>Here <span class="math">\(n_e n_{\mathrm{H}} V = \phi_{\mathrm{dust}} Q(\mathrm{H}^0)/ \alpha_{\mathrm{B}}(T)\)</span> from photoionization equilibrium, <span class="math">\(E_{nn'}\)</span> is the energy difference between hydrogen levels <span class="math">\(n\)</span> and <span class="math">\(n'\)</span>, and the remaining terms and their sources appearing in this equation are:</p>
<ul class="simple">
<li><span class="math">\(\gamma_{\mathrm{ff}}^{(\mathrm{H})}\)</span> and <span class="math">\(\gamma_{\mathrm{ff}}^{(\mathrm{He})}\)</span>: HII and HeII free-free emission coefficients; these are computed from eqution 10.1 of <a class="reference external" href="http://adsabs.harvard.edu/abs/2011piim.book.....D">Draine (2011)</a>, using the analytic approximation to the Gaunt factor given by equation 10.8 of the same source</li>
<li><span class="math">\(\gamma_{\mathrm{bf}}^{(\mathrm{H})}\)</span> and <span class="math">\(\gamma_{\mathrm{bf}}^{(\mathrm{He})}\)</span>: HI and HeI bound-free emission coefficients; these are computed using the tabulation and interpolation method given in <a class="reference external" href="http://adsabs.harvard.edu/abs/2006MNRAS.372.1875E">Ercolano &amp; Storey (2006, MNRAS, 372, 1875)</a></li>
<li><span class="math">\(\alpha_{nn'}^{\mathrm{eff,B,(H)}}\)</span> is the effective emission rate coefficient for the <span class="math">\(n\)</span> to <span class="math">\(n'\)</span> H recombination line, taken from the tabulation of <a class="reference external" href="http://adsabs.harvard.edu/abs/1995MNRAS.272...41S">Storey &amp; Hummer (1995, MNRAS, 272, 41)</a></li>
<li><span class="math">\(\gamma_{i,\mathrm{line}}^{(\mathrm{M})}\)</span> is the emissivity for the brightest non-hydrogen lines, computed using a set of pre-tabulated values, following the procedure described in the <a class="reference external" href="http://adsabs.harvard.edu/abs/2015MNRAS.452.1447K">SLUG 2 method paper</a></li>
<li><span class="math">\(\gamma_{\mathrm{2p}}^{(\mathrm{H})}\)</span>: hydrogen two-photon emissivity, computed as</li>
</ul>
<div class="math">
\[\gamma_{\mathrm{2p}}^{(\mathrm{H})} = \frac{hc}{\lambda^3} I(\mathrm{H}^0) \alpha_{2s}^{\mathrm{eff,(H)}} \frac{1}{1 + \frac{n_{\mathrm{H}} q_{2s-2p,p} + (1+x_{\mathrm{He}}) n_{\mathrm{H}} q_{2s-2p,e}}{A_{2s-1s}}} P_\nu\]</div>
<p>Here</p>
<blockquote>
<div><ul class="simple">
<li><span class="math">\(I(\mathrm{H}^0)\)</span> is the hydrogen ionization potential</li>
<li><span class="math">\(\alpha_{2s}^{\mathrm{eff,(H)}}\)</span> is the effective recombination rate to the 2s state, taken from the tabulation of <a class="reference external" href="http://adsabs.harvard.edu/abs/1995MNRAS.272...41S">Storey &amp; Hummer (1995, MNRAS, 272, 41)</a></li>
<li><span class="math">\(q_{2s-2p,p}\)</span> and <span class="math">\(q_{2s-2p,e}\)</span> are the collisional rate coefficients for transitions from the 2s to the 2p state induced by collisions with protons and electrons, respectively, taken from <a class="reference external" href="http://adsabs.harvard.edu/abs/1989agna.book.....O">Osterbrock (1989, University Science Books, table 4.10)</a></li>
<li><span class="math">\(A_{2s-1s}\)</span> is the Einstein coefficient for the hydrogen 2s-1s two-photon emission process, taken from <a class="reference external" href="http://adsabs.harvard.edu/abs/2011piim.book.....D">Draine (2011, section 14.2.4)</a></li>
<li><span class="math">\(P_\nu\)</span> is the frequency distribution for two-photon emission, computed from the analytic approximation of <a class="reference external" href="http://adsabs.harvard.edu/abs/1984A%26A...138..495N">Nussbaumer &amp; Schmutz (1984, A&amp;A, 138, 495)</a></li>
</ul>
</div></blockquote>
</div>
<div class="section" id="extinction">
<span id="ssec-extinction"></span><h2>Extinction<a class="headerlink" href="#extinction" title="Permalink to this headline">¶</a></h2>
<p>If extinction is enabled, SLUG applies extinction to the stellar spectra and, if nebular processing is enabled as well, to the spectrum that emerges from the nebula. Note that the nebular plus extincted spectrum computation is not fully self-consistent, in that the dust absorption factor <span class="math">\(\phi_{\mathrm{dust}}\)</span> used in the nebular emission calculation (see <a class="reference internal" href="#ssec-nebula"><span>Nebular Processing</span></a>) is not affected by the value of <span class="math">\(A_V\)</span> used in the calculation.</p>
<p>SLUG computes the extincted spectrum as</p>
<div class="math">
\[L_{\lambda,\mathrm{ex}} = L_{\lambda} e^{-\tau_\lambda}\]</div>
<p>where the optical depth <span class="math">\(\tau_\lambda = (\kappa_\lambda / \kappa_V) (A_V/1.086)\)</span>, <span class="math">\(A_V\)</span> is the visual extinction in mag, the factor 1.086 is the conversion between magnitudes and the true dimensionless optical depth, <span class="math">\(\kappa_\lambda\)</span> is a user-specified input extinction at wavelength <span class="math">\(\lambda\)</span>, and the V-band mean opacity is defined by</p>
<div class="math">
\[\kappa_V = \frac{\int \kappa_\nu R_\nu(V) \, d\nu}{\int R_\nu(V) \, d\nu}\]</div>
<p>where <span class="math">\(R_\nu(V)\)</span> is the filter response function as frequency <span class="math">\(\nu\)</span> for the Johnson V filter. The extinction curve <span class="math">\(\kappa_\lambda\)</span> can be specified via a user-provided file, or the user may select from a set of pre-defined extinction curves; see <a class="reference internal" href="parameters.html#ssec-ism-keywords"><span>Interstellar Medium Model Keywords</span></a> for details.</p>
<p>The computation for <span class="math">\(L_{\lambda,\mathrm{neb,ex}}\)</span> is analogous.</p>
</div>
</div>


          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
  <h3><a href="index.html">Table Of Contents</a></h3>
  <ul>
<li><a class="reference internal" href="#">Introduction to SLUG</a><ul>
<li><a class="reference internal" href="#what-does-slug-do">What Does SLUG Do?</a></li>
<li><a class="reference internal" href="#cluster-simulations-and-galaxy-simulations">Cluster Simulations and Galaxy Simulations</a></li>
<li><a class="reference internal" href="#probability-distribution-functions-the-imf-sfh-cmf-clf-a-v-distribution">Probability Distribution Functions: the IMF, SFH, CMF, CLF, A_V distribution</a></li>
<li><a class="reference internal" href="#spectra-and-photometry">Spectra and Photometry</a></li>
<li><a class="reference internal" href="#monte-carlo-simulation">Monte Carlo Simulation</a></li>
<li><a class="reference internal" href="#nebular-processing">Nebular Processing</a></li>
<li><a class="reference internal" href="#extinction">Extinction</a></li>
</ul>
</li>
</ul>

  <h4>Previous topic</h4>
  <p class="topless"><a href="getting.html"
                        title="previous chapter">Getting SLUG</a></p>
  <h4>Next topic</h4>
  <p class="topless"><a href="compiling.html"
                        title="next chapter">Compiling and Installing SLUG</a></p>
  <div role="note" aria-label="source link">
    <h3>This Page</h3>
    <ul class="this-page-menu">
      <li><a href="_sources/intro.txt"
            rel="nofollow">Show Source</a></li>
    </ul>
   </div>
<div id="searchbox" style="display: none" role="search">
  <h3>Quick search</h3>
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" />
      <input type="submit" value="Go" />
      <input type="hidden" name="check_keywords" value="yes" />
      <input type="hidden" name="area" value="default" />
    </form>
    <p class="searchtip" style="font-size: 90%">
    Enter search terms or a module, class or function name.
    </p>
</div>
<script type="text/javascript">$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &copy;2014, Mark Krumholz, Michele Fumagalli, et al..
      
      |
      Powered by <a href="http://sphinx-doc.org/">Sphinx 1.3.1</a>
      &amp; <a href="https://github.com/bitprophet/alabaster">Alabaster 0.7.2</a>
      
      |
      <a href="_sources/intro.txt"
          rel="nofollow">Page source</a></li>
    </div>

    

    
  </body>
</html>